* Modernisation complète du code de téléchargement, de l'indice, et de la base de données. Cela devrait être plus rapide et plus résilient lors de l'utilisation de la bande passante réduite. C'est aussi plus facile à maintenir avec une suite de tests grandement améliorée.

* Corrections : #649 #860 #1206 #1588 #1710 #1989 #2080 #2081 #2157 #2322 #2353 #2370
  #2412 #2436 #2442 #2443 #2444 #1971

* Nouvelle langue : le cantonais (yue)
